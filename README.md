# SUML Loader

![SUML logo](logo.png)

This is a Webpack loader for `*.suml` files.
SUML's specification is available at [gitlab.com/Avris/SUML](https://gitlab.com/Avris/SUML)

## Installation

    yarn add suml-loader
   
In `webpack.config.js` add rule:

    {
        test: /\.suml$/,
        loader: 'suml-loader',
    }

Now you can just `import foo from './foo.suml'`
