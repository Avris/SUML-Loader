const suml = new (require('suml'));

const replacer = function (key, value) {
    if (this[key] === Infinity) {
        return '<<<{{{Infinity}}}>>>';
    }
    if (this[key] === -Infinity) {
        return '<<<{{{-Infinity}}}>>>';
    }
    if (this[key] instanceof Date) {
        return `<<<{{{new Date('${this[key].toISOString()}')}}}>>>`;
    }
    return value;
};

module.exports = function(source) {
    return `export default ${JSON.stringify(suml.parse(source), replacer)}`
        .replace(/"<<<{{{([^}]*)}}}>>>"/g, '$1');
};
